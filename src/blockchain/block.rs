use hex;
use log::*;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::time::{SystemTime, UNIX_EPOCH};

const DIFFICULTY_PREFIX: [u8; 2] = [b'0', b'0'];

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Block {
    pub id: u64,
    pub hash: Vec<u8>,
    pub previous_hash: Vec<u8>,
    pub timestamp: u128,
    pub data: String,
    pub nonce: u64,
}

impl Block {
    pub fn new(previous: &Option<&Block>, data: &String) -> Self {
        Self::mine(previous, data)
    }

    pub fn is_valid(self: &Block, previous: &Block) -> bool {
        if self.previous_hash != previous.hash {
            warn!("Previous hash of block with id {} does not match the hash of the previous block with id {}", self.id, previous.id);
            return false;
        } else if !self.hash.starts_with(&DIFFICULTY_PREFIX[1..2]) {
            warn!("Block with id {} starts with invalid difficulty", self.id);
            return false;
        } else if self.id != previous.id + 1 {
            warn!("Block with id {} has an invalid id", self.id);
            return false;
        } else if Self::calculate_hash(
            self.id,
            self.timestamp,
            &self.previous_hash,
            &self.data,
            self.nonce,
        ) != self.hash
        {
            warn!("Block with id {} has an invalid hash", self.id);
            return false;
        }

        true
    }

    fn mine(previous: &Option<&Block>, data: &String) -> Block {
        info!("Mining block...");
        let mut nonce = 0;
        let timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards")
            .as_micros();
        let previous_hash = match previous {
            Some(p) => p.hash.to_owned(),
            None => String::from("genesis").into_bytes(),
        };
        let id = match previous {
            Some(p) => p.id + 1,
            None => 0,
        };

        loop {
            let hash = Self::calculate_hash(id, timestamp, &previous_hash, &data, nonce);
            if hash.starts_with(&DIFFICULTY_PREFIX) {
                info!(
                    "Mining complete! nonce: {}, hash: {}, ",
                    nonce,
                    hex::encode(data)
                );
                return Block {
                    id,
                    hash,
                    previous_hash,
                    timestamp,
                    data: data.to_owned(),
                    nonce,
                };
            }
            nonce += 1;
        }
    }

    pub fn calculate_hash(
        id: u64,
        timestamp: u128,
        previous_hash: &Vec<u8>,
        data: &String,
        nonce: u64,
    ) -> Vec<u8> {
        let data = serde_json::json!({
            "id": id,
            "previous_hash": previous_hash,
            "data": data,
            "timestamp": timestamp.to_string(), // Workaround as serde_json doesn't support u128
            "nonce": nonce
        });
        let mut hasher = Sha256::new();
        hasher.update(data.to_string().as_bytes());
        hasher.finalize().as_slice().to_owned()
    }
}
