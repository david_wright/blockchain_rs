use crate::blockchain::block::Block;

use adjacent_pair_iterator::AdjacentPairIterator;
use log::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Chain {
    pub blocks: Vec<Block>,
}

impl Chain {
    pub fn new() -> Self {
        Chain { blocks: vec![] }
    }

    pub fn try_add(&mut self, block: Block) -> bool {
        let latest = self.blocks.last();
        match latest {
            Some(l) => {
                if block.is_valid(l) {
                    self.blocks.push(block);
                    return true;
                } else {
                    error!("Block is invalid, could not add it");
                    return false;
                }
            }
            None => {
                self.blocks.push(block);
                return true;
            }
        }
    }

    pub fn is_valid(&self) -> bool {
        self.blocks
            .iter()
            .adjacent_pairs()
            .all(|(x1, x2)| x1.is_valid(x2))
    }

    pub fn should_replace(&mut self, remote: &Chain) -> bool {
        let is_local_valid = self.is_valid();
        let is_remote_valid = remote.is_valid();

        !is_local_valid && is_remote_valid && self.blocks.len() < remote.blocks.len()
    }

    pub fn replace(&mut self, remote: Chain) {
        self.blocks = remote.blocks;
    }

    pub fn print(&self) {
        info!("Blockchain:");
        info!(
            "{}",
            serde_json::to_string_pretty(&self).expect("can jsonify blockchain")
        );
    }
}
