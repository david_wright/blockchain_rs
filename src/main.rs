use crate::blockchain::{Block, Chain};
use crate::net::{Behaviour, BehaviourEvent};

use std::env;
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;

use log::*;
use tracing_subscriber::EnvFilter;

use libp2p::{
    futures::StreamExt, gossipsub, mdns, noise, swarm::Swarm, swarm::SwarmEvent, tcp, yamux,
};

use tokio::{io::AsyncBufReadExt, sync::mpsc};

use futures::lock::Mutex;
use futures_util::FutureExt;

pub mod blockchain;
pub mod net;

enum Event {
    InputEvent(String),
    SwarmEvent(SwarmEvent<BehaviourEvent>),
}

pub fn publish_command(
    swarm: &mut Swarm<Behaviour>,
    line: String,
    topic_commands: &gossipsub::IdentTopic,
) {
    if let Err(e) = swarm
        .behaviour_mut()
        .gossipsub
        .publish(topic_commands.clone(), line.as_bytes())
    {
        error!("Publish error: {e:?}");
    }
}

pub fn handle_swarm_event(
    event: &SwarmEvent<BehaviourEvent>,
    swarm: &mut Swarm<Behaviour>,
    chain: &mut blockchain::Chain,
    topic_commands: &gossipsub::IdentTopic,
    topic_blocks: &gossipsub::IdentTopic,
    topic_chains: &gossipsub::IdentTopic,
) {
    match event {
        SwarmEvent::Behaviour(BehaviourEvent::Mdns(mdns::Event::Discovered(list))) => {
            for (peer_id, _multiaddr) in list {
                swarm.behaviour_mut().gossipsub.add_explicit_peer(&peer_id);
            }
        }
        SwarmEvent::Behaviour(BehaviourEvent::Mdns(mdns::Event::Expired(list))) => {
            for (peer_id, _multiaddr) in list {
                swarm
                    .behaviour_mut()
                    .gossipsub
                    .remove_explicit_peer(&peer_id);
            }
        }
        SwarmEvent::Behaviour(BehaviourEvent::Gossipsub(gossipsub::Event::Message {
            propagation_source: peer_id,
            message_id: id,
            message,
        })) => match message.topic.clone() {
            _a if _a == topic_commands.clone().into() => {
                let command = String::from_utf8_lossy(&message.data);

                debug!("Received cmd: '{command}' with id: {id} from peer: {peer_id}");
                handle_command(
                    &command.to_string(),
                    swarm,
                    chain,
                    topic_commands,
                    topic_blocks,
                    topic_chains,
                );
            }
            _a if _a == topic_blocks.clone().into() => {
                info!("Received block from {peer_id}");
                match serde_json::from_slice(&message.data) {
                    Ok(block) => {
                        if handle_remote_block(block, chain) {
                            info!("Added block to chain");
                        } else {
                            error!("Received invalid block, asking peers to share their chains with us");
                            publish_command(swarm, "ls c".to_owned(), &topic_commands)
                        }
                    }
                    Err(e) => error!("Could not deserialise block: {e:?}"),
                }
            }
            _a if _a == topic_chains.clone().into() => {
                info!("Received chain from {peer_id}");

                match serde_json::from_slice(&message.data) {
                    Ok(remote_chain) => {
                        if handle_remote_chain(remote_chain, chain) {
                            info!("Replaced chain with received chain");
                        } else {
                            info!("Did not replace chain with received chain");
                        }
                    }
                    Err(e) => error!("Could not deserialise chain: {e:?}"),
                }
            }
            _ => {
                error!("Received unrecognised topic {}", &message.topic);
            }
        },
        SwarmEvent::NewListenAddr { address, .. } => {
            println!("Local node is listening on {address}");
        }
        _ => {}
    }
}

pub fn handle_command(
    command: &String,
    swarm: &mut Swarm<Behaviour>,
    chain: &mut blockchain::Chain,
    topic_commands: &gossipsub::IdentTopic,
    topic_blocks: &gossipsub::IdentTopic,
    topic_chains: &gossipsub::IdentTopic,
) {
    match command.as_ref() {
        "ls p" => swarm.behaviour().print_peers(),
        "ls c" => {
            chain.print();
            share_chain(swarm, chain, topic_chains.clone())
        }
        cmd if cmd.starts_with("create ") => {
            if handle_create_block(cmd.strip_prefix("create ").unwrap(), chain) {
                let block = chain
                    .blocks
                    .last()
                    .expect("at least one block after creating a block");
                let json = serde_json::to_string(&block).expect("that we can serialise a block");
                debug!("Broadcasting new block");

                swarm
                    .behaviour_mut()
                    .gossipsub
                    .publish((*topic_blocks).clone(), json);
            } else {
                error!("Could not add block to chain");
            }
        }
        _ => error!("unknown command"),
    }
}

pub fn handle_create_block(data: &str, local_chain: &mut blockchain::Chain) -> bool {
    let latest_block = local_chain.blocks.last();
    let block = Block::new(&latest_block, &data.to_string());
    local_chain.try_add(block)
}

pub fn handle_remote_block(remote_block: Block, local_chain: &mut blockchain::Chain) -> bool {
    local_chain.try_add(remote_block)
}

pub fn handle_remote_chain(remote_chain: Chain, local_chain: &mut blockchain::Chain) -> bool {
    if local_chain.should_replace(&remote_chain) {
        local_chain.replace(remote_chain);
        true
    } else {
        false
    }
}

pub fn share_chain(
    swarm: &mut Swarm<Behaviour>,
    chain: &blockchain::Chain,
    topic: gossipsub::IdentTopic,
) {
    match serde_json::to_string(&chain) {
        Ok(serialised_chain) => {
            swarm
                .behaviour_mut()
                .gossipsub
                .publish(topic.clone(), serialised_chain);
        }
        Err(e) => error!("Could not deserialise chain: {e:?}"),
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    match env::var("RUST_LOG") {
        Ok(v) => println!("Logging with log level '{v}' provided by the user"),
        Err(e) => {
            println!("Logging with default log level 'info'");
            env::set_var("RUST_LOG", "info")
        }
    }

    pretty_env_logger::init();

    let _ = tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .try_init();

    // Create a Gossipsub topic
    let topic_blocks = gossipsub::IdentTopic::new("blocks");
    let topic_chains = gossipsub::IdentTopic::new("chains");
    let topic_commands = gossipsub::IdentTopic::new("commands");

    let mut swarm = Arc::new(Mutex::new(
        libp2p::SwarmBuilder::with_new_identity()
            .with_tokio()
            .with_tcp(
                tcp::Config::default(),
                noise::Config::new,
                yamux::Config::default,
            )?
            .with_quic()
            .with_behaviour(|key| {
                // Set a custom gossipsub configuration
                let gossipsub_config = gossipsub::ConfigBuilder::default()
                    .heartbeat_interval(Duration::from_secs(10)) // This is set to aid debugging by not cluttering the log space
                    .validation_mode(gossipsub::ValidationMode::Strict) // This sets the kind of message validation. The default is Strict (enforce message signing)
                    .build()
                    .map_err(|msg| tokio::io::Error::new(tokio::io::ErrorKind::Other, msg))?; // Temporary hack because `build` does not return a proper `std::error::Error`.

                // build a gossipsub network behaviour
                let gossipsub = gossipsub::Behaviour::new(
                    gossipsub::MessageAuthenticity::Signed(key.clone()),
                    gossipsub_config,
                )?;

                let mdns = mdns::tokio::Behaviour::new(
                    mdns::Config::default(),
                    key.public().to_peer_id(),
                )?;
                Ok(Behaviour { gossipsub, mdns })
            })?
            .with_swarm_config(|c| c.with_idle_connection_timeout(Duration::from_secs(60)))
            .build(),
    ));

    let mut chain = Chain::new();

    swarm
        .lock()
        .await
        .behaviour_mut()
        .gossipsub
        .subscribe(&topic_blocks)?;
    swarm
        .lock()
        .await
        .behaviour_mut()
        .gossipsub
        .subscribe(&topic_chains)?;
    swarm
        .lock()
        .await
        .behaviour_mut()
        .gossipsub
        .subscribe(&topic_commands)?;

    // Listen on all interfaces and whatever port the OS assigns
    swarm
        .lock()
        .await
        .listen_on("/ip4/0.0.0.0/udp/0/quic-v1".parse()?)?;
    swarm
        .lock()
        .await
        .listen_on("/ip4/0.0.0.0/tcp/0".parse()?)?;

    // Read full lines from stdin
    let mut stdin = tokio::io::BufReader::new(tokio::io::stdin()).lines();
    println!("Enter messages via STDIN and they will be sent to connected peers using Gossipsub");

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<Event>();
    let tx1 = tx.clone();

    tokio::spawn(async move {
        loop {
            match stdin.next_line().await {
                Ok(line) => match line {
                    Some(line) => {
                        debug!("Read line from stdin: {line}");
                        tx1.send(Event::InputEvent(line));
                    }
                    None => {
                        error!("Nothing to read from stdin.");
                    }
                },
                Err(e) => {
                    error!("Could not read line from stdin: {e:?}");
                }
            }
        }
    });

    let (swarm2, tx2) = (swarm.clone(), tx.clone());
    tokio::spawn(async move {
        loop {
            tx2.send(Event::SwarmEvent(
                swarm2.lock().await.select_next_some().await,
            ));
        }
    });

    loop {
        match rx.recv().await {
            Some(event) => match event {
                Event::InputEvent(line) => {
                    let mut swarm = swarm.lock().await;
                    publish_command(&mut swarm, line, &topic_commands);
                }
                Event::SwarmEvent(event) => {
                    let mut swarm = swarm.lock().await;
                    handle_swarm_event(
                        &event,
                        &mut swarm,
                        &mut chain,
                        &topic_commands,
                        &topic_blocks,
                        &topic_chains,
                    );
                }
            },
            None => {
                debug!("No event received from channel");
            }
        }
    }
}
