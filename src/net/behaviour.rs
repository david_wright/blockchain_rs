use libp2p::{gossipsub, mdns, swarm::NetworkBehaviour};

use log::*;

use std::collections::HashSet;

// For the NetworkBehaviour docs, see: https://docs.rs/libp2p/latest/libp2p/swarm/trait.NetworkBehaviour.html
#[derive(NetworkBehaviour)]
pub struct Behaviour {
    pub gossipsub: gossipsub::Behaviour,
    pub mdns: mdns::tokio::Behaviour,
}

impl Behaviour {
    pub fn get_peers(&self) -> Vec<String> {
        let nodes = self.mdns.discovered_nodes();
        let mut unique_peers = HashSet::new();
        for peer in nodes {
            unique_peers.insert(peer);
        }
        unique_peers.iter().map(|p| p.to_string()).collect()
    }

    pub fn print_peers(&self) {
        info!("Peers:");
        let peers = self.get_peers();
        for peer in peers {
            info!("\t{}", peer);
        }
    }
}
