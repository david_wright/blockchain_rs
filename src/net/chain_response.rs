use serde::{Deserialize, Serialize};

use crate::blockchain::Chain;

#[derive(Debug, Serialize, Deserialize)]
pub struct ChainResponse {
    pub chain: Chain,
    pub receiver: String,
}
